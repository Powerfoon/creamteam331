/**
 * @file heap.h
 * @author Jonathan Chuhn
 * @brief defines a heap structure based on vectors
 *
 * This class manages data in a heap data structure, in a template class manner.
 * Data types managed should have appropriately defined comparator functions.
 * This heap is a min heap, meaning the root is the minimum and numbers increase
 * going down the tree.
 */
#ifndef _HEAP_H
#define _HEAP_H

#include <cstddef>
#include <limits.h>
#include <vector>
#include <fstream>
#include <iostream>       // std::cout
#include <string>         // std::string
#include "fileData.h"


#define DEFAULT_HEAP_SIZE 8  ///< The default length of the vector holding the heap
/** An in-line function to determine the location of the current node's parent. */
#define FIND_PARENT(i) ((i) / 2) + ((i) % 2 - 1)
/** An in-line function to determine the location of the current node's left child. */
#define FIND_CHILD_L(i) (i)*2 + 1
/** An in-line function to determine the location of the current node's right child. */
#define FIND_CHILD_R(i) (i)*2 + 2

template<typename T>
class heap {
private:
    std::vector<T> _heap;  ///< Vector to hold heap data

    void swap(T &object1, T &object2);  ///< Function to swap two objects in heap

    /**
     * @brief builds a copy of the parameter vector as a heap
     * @param data
     * @post a heap of the data vector param
     *
     * A private method which takes the data held in data, and builds a
     * min heap stored in a vector, moving each value up until it is smaller
     * than its children.
     */
    std::vector<T> buildHeap(std::vector<T> data);

public:

	/** Constructors */
	heap(); ///< Default constructor
	heap(size_t maxSize);  ///< constructor with defined size
    heap(std::vector<T> initValues);  ///< constructor with initial values

	/** Public Functions */
    /**
     * @brief Adds newElement to the heap.
     *
     * Adds the newElement to end of the heap before organizing it
     * by bubbling it up until it is greater than its children.
     */
    void heapify(T newElement);

    /**
     * @brief Pops the root element from the heap.
     *
     * After popping the root element, it reorganizes the tree
     * to find a new root value, bubbling up the last value on the tree
     * to its proper place.
     */
    T popRoot();

    /// Returns value of the node pointed to by the parameter index.
    T getNode(int node);

    /// Returns status on whether or not heap is empty
    bool isEmpty();


};

#include "heap.cpp"
#endif /* _HEAP_H */
