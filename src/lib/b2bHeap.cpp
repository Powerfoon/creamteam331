/**
 * @file fileData.cpp
 * @author Jonathan Chuhn
 * @brief Implementation file for b2bHeap class
 */
#include "b2bHeap.h"

template<typename T>
b2bHeap<T>::b2bHeap() {
    _heap.resize(DEFAULT_CURRENT_HEAP_SIZE);
    currentHeapSize = DEFAULT_CURRENT_HEAP_SIZE;
    pendingHeapSize = DEFAULT_PENDING_HEAP_SIZE;
    maxIndex        = DEFAULT_CURRENT_HEAP_SIZE - 1;
    directionFlag   = true;
    frontHeapEnd    = START;
    backHeapEnd     = DEFAULT_CURRENT_HEAP_SIZE;
}

template<typename T>
b2bHeap<T>::b2bHeap(int size) {
    _heap.resize(size);
    currentHeapSize = size;
    pendingHeapSize = DEFAULT_PENDING_HEAP_SIZE;
    maxIndex        = size - 1;
    directionFlag   = true;
    frontHeapEnd    = START;
    backHeapEnd     = size;
}

template<typename T>
bool b2bHeap<T>::current_heap_push(T newElement) {
    int currentNode;
    int parentNode;
    bool isAdded;

    if (directionFlag) {
        if (frontHeapEnd < currentHeapSize - 1) {
            frontHeapEnd++;
            _heap[frontHeapEnd] = newElement;
            currentNode         = frontHeapEnd;
            parentNode          = FIND_PARENT(currentNode);

            while (currentNode != FIRST && _heap[currentNode] < _heap[parentNode]){
                swap(_heap[currentNode], _heap[parentNode]);
                currentNode = parentNode;
                parentNode  = FIND_PARENT(currentNode);
            }

            isAdded = true;
        }
        else {
            isAdded = false;
        }
    }
    else {
        if (backHeapEnd > pendingHeapSize) {
            backHeapEnd--;
            _heap[backHeapEnd] = newElement;
            currentNode        = backHeapEnd;
            parentNode         = maxIndex - FIND_PARENT(maxIndex - currentNode);

            while (currentNode != maxIndex && _heap[currentNode] < _heap[parentNode]){
                swap(_heap[currentNode], _heap[parentNode]);
                currentNode = parentNode;
                parentNode  = maxIndex - FIND_PARENT(maxIndex - currentNode);
            }
            isAdded = true;
        }
        else {
            isAdded = false;
        }
    }

    return isAdded;
}

template<typename T>
bool b2bHeap<T>::pending_heap_push(T newElement) {
    int currentNode;
    int parentNode;
    bool isAdded;

    if (!directionFlag) {
        if (frontHeapEnd < pendingHeapSize - 1) {
            frontHeapEnd++;
            _heap[frontHeapEnd] = newElement;
            currentNode         = frontHeapEnd;
            parentNode          = FIND_PARENT(currentNode);

            while (currentNode != FIRST && _heap[currentNode] < _heap[parentNode]){
                swap(_heap[currentNode], _heap[parentNode]);
                currentNode = parentNode;
                parentNode  = FIND_PARENT(currentNode);
            }

            isAdded = true;
        }
        else {
            isAdded = false;
        }
    }
    else {
        if (backHeapEnd > currentHeapSize) {
            backHeapEnd--;
            _heap[backHeapEnd] = newElement;
            currentNode        = backHeapEnd;
            parentNode         = maxIndex - FIND_PARENT(maxIndex - currentNode);

            while (currentNode != maxIndex && _heap[currentNode] < _heap[parentNode]){
                swap(_heap[currentNode], _heap[parentNode]);
                currentNode = parentNode;
                parentNode  = maxIndex - FIND_PARENT(maxIndex - currentNode);
            }
            isAdded = true;
        }
        else {
            isAdded = false;
        }
    }

    return isAdded;
}

template<typename T>
T b2bHeap<T>::current_heap_pop() {
    T nullElem;
    T poppedElem = nullElem;
    int currentNode;
    int childL;
    int childR;
    int smallestChild;

    if (directionFlag && frontHeapEnd > START) {
        currentNode = FIRST;
        childL      = LEFT;
        childR      = RIGHT;

        swap(_heap[currentNode], _heap[frontHeapEnd]);
        poppedElem = _heap[frontHeapEnd];
        _heap[frontHeapEnd] = nullElem;

        while (childR < frontHeapEnd || childL < frontHeapEnd) {
            if (childL < frontHeapEnd && childR < frontHeapEnd) {
                smallestChild = (_heap[childL] < _heap[childR]) ? childL : childR;

                if (_heap[currentNode] > _heap[smallestChild]) {
                    swap(_heap[currentNode], _heap[smallestChild]);
                    currentNode = smallestChild;
                    childL      = FIND_CHILD_L(currentNode);
                    childR      = FIND_CHILD_R(currentNode);
                }
                else {
                    break;
                }

            }
            else {
                if (_heap[currentNode] > _heap[childL]) {
                    swap(_heap[currentNode], _heap[childL]);
                }

                break;
            }
        }

        frontHeapEnd--;
    }
    else if (!directionFlag && backHeapEnd <= maxIndex) {
        currentNode = maxIndex;
        childL      = maxIndex - LEFT;
        childR      = maxIndex - RIGHT;

        swap(_heap[currentNode], _heap[backHeapEnd]);
        poppedElem = _heap[backHeapEnd];
        _heap[backHeapEnd] = nullElem;

        while (childR > backHeapEnd || childL > backHeapEnd) {
            if (childL > backHeapEnd && childR > backHeapEnd) {
                smallestChild = (_heap[childL] < _heap[childR]) ? childL : childR;

                if (_heap[currentNode] > _heap[smallestChild]) {
                    swap(_heap[currentNode], _heap[smallestChild]);
                    currentNode = smallestChild;
                    childL      = maxIndex - FIND_CHILD_L(maxIndex - smallestChild);
                    childR      = maxIndex - FIND_CHILD_R(maxIndex - smallestChild);
                }
                else {
                    break;
                }
            }
            else {
                if (_heap[currentNode] > _heap[childL]) {
                    swap(_heap[currentNode], _heap[childL]);
                }

                break;
            }
        }

        backHeapEnd++;
    }

    return poppedElem;
}

template<typename T>
void b2bHeap<T>::increase_pending() {
    T nullElem;

    if (pendingHeapSize > maxIndex)
        return;

    pendingHeapSize++;
    currentHeapSize--;

    if (directionFlag) {
        if (frontHeapEnd >= currentHeapSize)
            frontHeapEnd = currentHeapSize - 1;

        _heap[currentHeapSize] = nullElem;
    }
    else {
        if (backHeapEnd <= maxIndex - currentHeapSize)
            backHeapEnd = maxIndex - currentHeapSize + 1;

        _heap[maxIndex - currentHeapSize] = nullElem;
    }
}

template<typename T>
void b2bHeap<T>::switch_heap() {
    int tempSize;

    directionFlag = !directionFlag;
    tempSize = currentHeapSize;
    currentHeapSize = pendingHeapSize;
    pendingHeapSize = tempSize;
}

template<typename T>
bool b2bHeap<T>::currIsFull() {
    bool status = false;
    if (directionFlag) {
        status = (frontHeapEnd == currentHeapSize - 1);
    }
    else {
        status = (backHeapEnd == maxIndex - currentHeapSize + 1);
    }

    return status;
}

template<typename T>
void b2bHeap<T>::reset() {
    _heap.clear();

    currentHeapSize = maxIndex + 1;
    pendingHeapSize = DEFAULT_PENDING_HEAP_SIZE;

    backHeapEnd     = maxIndex + 1;
    frontHeapEnd    = START;

    directionFlag   = true;

}

template<typename T>
bool b2bHeap<T>::currIsEmpty() {
    bool status = false;

    status = directionFlag ? (frontHeapEnd == START) : (backHeapEnd == maxIndex + 1);

    return status;
}

template<typename T>
bool b2bHeap<T>::isEmpty() {
    bool status = false;

    status = (frontHeapEnd == START) && (backHeapEnd == maxIndex + 1);

    return status;
}

template<typename T>
int b2bHeap<T>::currentSize() {
    return currentHeapSize;
};

template<typename T>
int b2bHeap<T>::pendingSize() {
    return pendingHeapSize;
};

/** PRIVATE FUNCTIONS */
template<typename T>
void b2bHeap<T>::swap(T &object1, T &object2) {
    T temp;

    temp    = object1;
    object1 = object2;
    object2 = temp;
}
