#include "heap.h"

template<typename T>
heap<T>::heap() {
    _heap.resize(DEFAULT_HEAP_SIZE);
}

template<typename T>
heap<T>::heap(size_t size) {
    _heap.resize(size);
};

template<typename T>
heap<T>::heap(std::vector<T> initValues) {
    _heap = buildHeap(initValues);
};

template<typename T>
void heap<T>::heapify(T newElement) {
    int currentNode, parentNode;

    _heap.push_back(newElement);
    currentNode = _heap.size();
    parentNode = FIND_PARENT(currentNode);

    // Bubble Up lower values
    while (currentNode != 0 && _heap[currentNode] < _heap[parentNode]){
        swap(_heap[currentNode], _heap[parentNode]);
        currentNode = parentNode;
        parentNode  = FIND_PARENT(currentNode);
    }
}

template<typename T>
T heap<T>::popRoot() {
    T poppedElem;
    int currentNode  = 0;
    int childL       = FIND_CHILD_L(currentNode);
    int childR       = FIND_CHILD_R(currentNode);
    int smallestChild;
    size_t lastIndex = _heap.size() - 1;

    swap(_heap[0], _heap[lastIndex]);
    poppedElem = _heap[lastIndex];
    _heap.pop_back();

    //Bubble Down the swapped value
    while (childR < lastIndex || childL < lastIndex) {
        if (childL < lastIndex && childR < lastIndex) {
            smallestChild = (_heap[childL] < _heap[childR]) ? childL : childR;

            if (_heap[currentNode] > _heap[smallestChild]) {
                swap(_heap[currentNode], _heap[smallestChild]);
                currentNode = smallestChild;
                childL      = FIND_CHILD_L(currentNode);
                childR      = FIND_CHILD_R(currentNode);
            }
            else {
                break;
            }
        }
        else { //childR doesn't exist, only compare to childL
            if (_heap[currentNode] > _heap[childL]) {
                swap(_heap[currentNode], _heap[childL]);
            }

            break;
        }
    }

    return poppedElem;
}

template<typename T>
T heap<T>::getNode(int node) {
    return _heap[node];
}

template<typename T>
bool heap<T>::isEmpty() {
    return _heap.empty();
}

/** PRIVATE FUNCTIONS */

template<typename T>
std::vector<T> heap<T>::buildHeap(std::vector<T> dataVector) {
    std::vector<T> newHeap;
    int parentNode;
    int currentNode;

    if (dataVector.size() < 1)
        return newHeap;

    newHeap.push_back(dataVector[0]);

    for (int i = 1; i < dataVector.size(); i++) {
        newHeap.push_back(dataVector[i]);
        currentNode = i;
        parentNode = FIND_PARENT(i);

        // Bubble Up lower values
        while (currentNode != 0 && newHeap[currentNode] < newHeap[parentNode]){
            swap(newHeap[currentNode], newHeap[parentNode]);
            currentNode = parentNode;
            parentNode  = FIND_PARENT(currentNode);
        }
    }

    return newHeap;
}

template<typename T>
void heap<T>::swap(T &object1, T &object2) {
    T temp;

    temp    = object1;
    object1 = object2;
    object2 = temp;
}
