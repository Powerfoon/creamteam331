/**
 * @file fileData.h
 * @author Jonathan Chunh, Benjamin Jacobs
 * @brief fileData defines datatype to hold order pairs of ID and Name
 *
 * This class defines all of the necessary operators for comparing and setting
 * a data type made of both integers and strings. In general, the integers are
 * compared before the strings.
 */
#ifndef _FILEDATA_H
#define _FILEDATA_H

#include <stddef.h>
#include <string>
#include <cstring>
#include <limits.h>

using namespace std;

class fileData{




public:
    int    id;       ///< Contains number id of the data.
    string name;     ///< Contains strings of name of the data.
    bool   sortInt;  ///< If true, sorts by ID number.
    bool   sortName; ///< If true, sorts by the name. Overridden by sortInt.

    /**
     * Default constructor for fileData type. Initialized with ordered-pair sorting,
     * ID = 0, and name = ""
     * @post Creates a fileData object with empty members
     */
    fileData();

    /**
     * Default constructor for fileData type. Initialized with ordered-pair sorting
     * and specified ID and name.
     * @param id Integer value ID of ordered pair
     * @param name String value of the name associated with the ordered pair
     * @post Creates a fileData object with ordered-pair sorting and given member values
     */
    fileData(int id, string name);

    /**
     * Greater than operator overload. Compares based on the sortInt flag and sortName flag.
     * If both flags are false (default), they are compared as ordered pair using ID first, then
     * name. If sortInt is true, this only compares the ID. If sortName is true, this only compares
     * the name. sortInt has higher priority than sortName.
     */
    bool operator > (const fileData& data2);

    /**
     * Less than operator overload. Compares based on the sortInt flag and sortName flag.
     * If both flags are false (default), they are compared as ordered pair using ID first, then
     * name. If sortInt is true, this only compares the ID. If sortName is true, this only compares
     * the name. sortInt has higher priority than sortName.
     */
    bool operator < (const fileData& data2);

    /**
     * equals-comparison operator overload. Compares based on the sortInt flag and sortName flag.
     * If both flags are false (default), they are compared as ordered pair using ID and name.
     * If sortInt is true, this only compares the ID. If sortName is true, this only compares
     * the name. sortInt has higher priority than sortName.
     */
    bool operator == (const fileData& data2);

    /**
     * Not equal assignment operator overload. Compares based on the sortInt flag and sortName flag.
     * If both flags are false (default), they are compared as ordered pair using ID and name.
     * If sortInt is true, this only compares the ID. If sortName is true, this only compares
     * the name. sortInt has higher priority than sortName.
     */
    bool operator != (const fileData& data2);

    /**
     * Equality assignment operator overload. Assigns all member variables of right operand to
     * the left operand.
     */
    fileData& operator =  (const fileData& data2);

    /**
     * This is reset some private variables to be the biggest value for that data type
     */
    void clear_data();
};

#include "fileData.cpp"
#endif /* _FILEDATA_H */
