/**
 * @file b2bHeap.h
 * @author Jonathan Chunh
 * @brief b2bHeap.h defines the operations and structures for a back-to-back heap.
 */
#ifndef _B2BHEAP_H
#define _B2BHEAP_H

#include <cstddef>
#include <vector>

#define FIRST 0
#define LEFT  1
#define RIGHT 2
#define START -1

#define DEFAULT_CURRENT_HEAP_SIZE 8
#define DEFAULT_PENDING_HEAP_SIZE 0


#define FIND_PARENT(i)  (int) (((i) / 2) + ((i) % 2 - 1))
#define FIND_CHILD_L(i) (int) ((i) * 2 + 1)
#define FIND_CHILD_R(i) (int) ((i) * 2 + 2)

using namespace std;

template<typename T>
class b2bHeap {
private:
    vector<T> _heap;     ///< The vector containing the heaps
    int currentHeapSize; ///< The size of the active heap
    int pendingHeapSize; ///< The size of the pending heap
    bool directionFlag;  ///< Used to toggle between active and pending heaps
    int frontHeapEnd;    ///< The current end node of the front heap
    int backHeapEnd;     ///< The current end node of the back heap
    int maxIndex;        ///< The maximum index of the vector _heap

    
    void swap(T &object1, T &object2);

public:

    /**
     * @brief Constructor for default size 8 back-to-back heap.
     * @post Creates a back-to-back heap with max total size of 8.
     */
    b2bHeap();
    
    /**
     * @brief Constructor for variable max size heap.
     * @post Creates a back-to-back heap with max total size passed in.
     * @param size Total size of the heap object
     */
    b2bHeap(int size);
    
	/** Public Functions */
    
    /**
     * @brief Pushes an element into the current heap
     * @param[in] newElement the new element to be added
     */
    bool current_heap_push(T newElement);

    /**
     * @brief Pushes a node element onto the pending heap
     * @param[in] newElement the new node data to be pushed onto the heap
     */
    bool pending_heap_push(T newElement);

    /**
     * @brief Pops the root of the current node and reheapifies the heap.
     * @return The popped element
     */
    T current_heap_pop();

    /**
     * @brief Increases pending heap size and decreases active heap size.
     */
    void increase_pending();
    
    /**
     * @brief Switches the pending heap to active and active heap to pending.
     */
    void switch_heap();
    
    /**
     * @brief Resets the heap as if it was reinitialized.
     */
    void reset();
    
    /**
     * @brief Checks if the entire heap (Active and pending) are empty.
     * @return true if both active and pending heaps are empty.
     */
    bool isEmpty();
    
    /**
     * @brief Checks if the active heap is full
     * @return true if all active heap nodes are occupied
     */
    bool currIsFull();
    
    /**
     * @brief Checks if the active heap is empty
     * @return true if the active heap is currently empty
     */
    bool currIsEmpty();

    /**
     * @brief Gets the max size of the current heap
     * @return Maximum size of the current heap
     */
    int currentSize();
    
    /**
     * @brief Gets the max size of the pending heap
     * @return Maximum size of the pending heap
     */
    int pendingSize();
};

#include "b2bHeap.cpp"
#endif /* _B2BHEAP_H */
