/**
 * @file fileData.cpp

 * @author Jonathan Chuhn, Benjamin Jacobs
 * @brief Implementation file for fileData class
 */
#include "fileData.h"

fileData::fileData() {
    this->id       = 0;
    this->name     = "";
    this->sortInt  = false;
    this->sortName = false;
};

fileData::fileData(int id, string name) {
    this->id       = id;
    this->name     = name;
    this->sortInt  = false;
    this->sortName = false;
};

bool fileData::operator >  (const fileData& data2) {
    bool retVal = false;

    if (sortInt) {
        if (this->id > data2.id)
            retVal = true;
    }
    else if (sortName) {
        if (this->name > data2.name)
            retVal = true;
    }
    else {
        if (this->id > data2.id)
            retVal = true;
        else if (this->id == data2.id)
            if (this->name > data2.name)
                retVal = true;
    }

    return retVal;
}

bool fileData::operator <  (const fileData& data2) {
    bool retVal = false;

    if (sortInt) {
        if (this->id < data2.id)
            retVal = true;
    }
    else if (sortName) {
        if (this->name < data2.name)
            retVal = true;
    }
    else {
        if (this->id < data2.id)
            retVal = true;
        else if (this->id == data2.id)
            if (this->name < data2.name)
                retVal = true;
    }

    return retVal;
};

bool fileData::operator == (const fileData& data2) {
    bool retVal = false;

    if (sortInt) {
        if (this->id == data2.id)
            retVal = true;
    }
    else if (sortName) {
        if (this->name == data2.name)
            retVal = true;
    }
    else {
        if (this->id == data2.id && this->name == data2.name)
            retVal = true;
    }

    return retVal;
};

bool fileData::operator != (const fileData& data2) {
    bool retVal = false;

    if (sortInt) {
        if (this->id != data2.id)
            retVal = true;
    }
    else if (sortName) {
        if (this->name != data2.name)
            retVal = true;
    }
    else {
        if (this->id != data2.id || this->name != data2.name)
            retVal = true;
    }

    return retVal;
};

fileData& fileData::operator = (const fileData& data2) {
    this->id       = data2.id;
    this->name     = data2.name;
    this->sortInt  = data2.sortInt;
    this->sortName = data2.sortName;

    return *this;
};

void fileData::clear_data() {
    this->id      = INT_MAX;
    this->name    = "zzzzzzzzzz";
}
