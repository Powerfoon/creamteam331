#include <stdio.h>
#include <vector>
#include "../lib/b2bHeap.h"

#define TEST_WALL 6
#define MAX_HEAP_SIZE 10

int main (int argc, char **argv) {
    std::vector<int> testVector1 = {13, 3, 1, 6,14, 8, 5, 2, 9,11};
    std::vector<int> testVector2 = { 4, 5, 1, 6, 2, 9, 8, 7, 3, 0};
    std::vector<int> testVector3 = {41,12,16,33,67,99,84,73,22, 4};
    
    b2bHeap<int> testHeap1(MAX_HEAP_SIZE);
    b2bHeap<int> testHeap2(MAX_HEAP_SIZE);
    b2bHeap<int> testHeap3(MAX_HEAP_SIZE);
    
    printf("Finished Initializing...\n");
    
    /** Test for vector 1 to left side heap*/
    printf("Now testing Heaps...\n");
    for (int i = 0; i < MAX_HEAP_SIZE; i++) {
        testHeap1.current_heap_push(testVector1[i]);
    }
    
    printf("testHeap1: ");
    for (int i = 0; i < MAX_HEAP_SIZE; i++) {
        printf("%d ", testHeap1.current_heap_pop());
    }
    printf("\n");
    
    /** Test for vector1 to right side heap */
    /** Turn the entire testHeap2 to right sided heap */
    for (int i = 0; i < MAX_HEAP_SIZE; i++) {
        testHeap2.increase_pending();
    }
    
    for (int i = 0; i < MAX_HEAP_SIZE; i++) {
        testHeap2.pending_heap_push(testVector1[i]);
    }
    
    testHeap2.switch_heap(); //Switch to right-side heap as current
    
    printf("testHeap2: ");
    for (int i = 0; i < MAX_HEAP_SIZE; i++) {
        printf("%d ", testHeap2.current_heap_pop());
    }
    printf("\n");
    
    /** vector2 -> heap3 left-side | vector3 -> heap3 right-side */
    for (int i = 0; i < TEST_WALL; i++) {
        testHeap3.current_heap_push(testVector2[i]);
    }
    
    for (int i = 0; i < MAX_HEAP_SIZE - TEST_WALL; i++) {
        testHeap3.increase_pending();
        testHeap3.pending_heap_push(testVector3[i]);
    }
    
    printf("testHeap3:");
    for (int i = 0; i < TEST_WALL; i++) {
        printf(" %d", testHeap3.current_heap_pop());
    }
    printf(" |");
    
    testHeap3.switch_heap();
    for (int i = 0; i < MAX_HEAP_SIZE - TEST_WALL; i++) {
        printf(" %d", testHeap3.current_heap_pop());
    }
    printf("\n");
    
    printf("Done...\n");
    return 0;
}

