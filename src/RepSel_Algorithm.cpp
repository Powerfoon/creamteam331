/**
 * @mainpage
 * @file RepSel_Algorithm.cpp
 * @author Jonathan Chunh, Benjamin Jacobs
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>
#include <limits.h>
#include "lib/b2bHeap.h"
#include "lib/fileData.h"

#define DEFAULT_CONTAINER_SIZE 8            ///< Default size of heap for sorting.
#define MAX_CONTAINER_SIZE     134217729    ///< Maximum container size.
#define DEFAULT_SORT_OPTION    false        ///< Default boolean for sorting by ID.
#define STR_SIZE               128          ///< Default size for c-strings for filenames.

#define SUCCESS     0
#define ERR_ARG_CNT 1       ///< Signifies that the number of arguments is too small.
#define ERR_ARG_VAL 2       ///< Return value for when arguments aren't signified with flag.

#define MIN_ARG_CNT 3       ///< Minimum number of arguments required to run.

#define ZERO 0              ///< Constant value of 0
#define ONE  1              ///< Constant value of 1


using namespace std;

template <typename T>
void mergeKWay(std::vector<T> rrn, std::vector<int> numOfRecVector, int num_of_runs, int record_in_runs, int total_runs, int largeRec, ifstream &in_file, ofstream &out_file);

char repselFilename[STR_SIZE] = "results/repsel";       ///< Filename for replacement selection
char kwayFilename[STR_SIZE]   = "results/kwaysort.txt"; ///< Filname for kway sorting
char loggerFilename[STR_SIZE] = "results/log.txt";      ///< Filename for statistics log

int  containerSize = DEFAULT_CONTAINER_SIZE; ///< Size of heap used for sorting
bool sortByID      = DEFAULT_SORT_OPTION;    ///< Flag to sort by ID
bool sortByName    = DEFAULT_SORT_OPTION;    ///< Flag to sort by name
char filename[STR_SIZE];                     ///< Filename for input data

b2bHeap<fileData> container(containerSize); ///< Heap to sort input, of size containerSize
vector<fileData>  currentSorted;            ///< Array of a run's sorted records
fileData          currentResult;            ///< Holds the current data from filestream
fileData          lastResult;               ///< Holds the last sorted data

ofstream writeFile;                 ///< File stream for output file
ifstream readFile;                  ///< Filestream for input file
fileData data;                      ///< Buffer for read data
fileData dataOut;                   ///< Data printed out to result file


float averageNumOfRec = ZERO;       ///< Average number of records per run
int   numOfRuns       = ZERO;       ///< Holds total number of runs
int	  totalRec        = ZERO;       ///< Total number of records read

int         smallestRec = INT_MAX;  ///< smallest number of records in a run
int         largestRec  = ZERO;     ///< largest number of records in a run
int         numOfData   = ZERO;     ///< Number of data points for the current run
vector<int> numOfRecVector;

vector<long> rrnVect; ///<vector containing the RRN of each run

/**
* <h1>SYNOPSIS</h1>
* <p>Runs a replacement selection sort algorithm on a given file. The provided file
* should contain an integer number and a string representing an ID and a name
* followed by a newline.</p>
* <br>
* Below are three example records:<br>
* 1234 John<br>
* 54 Mary<br>
* 1938 Dylan<br>
* <br><br>
* <h2>ARGUMENTS:</h2>
* -d | dir <pathname><br>
*      (required)  The pathname to the file containing the records<br>
*<br>
* -sortid<br>
*      (optional)  Flag which specifies to sort the files by ID. Defaults to
*                  sorting as ordered pair.<br>
*<br>
* -sortname<br>
*      (optional)  Flag which specifies to sort the files by name. Defaults to
*                  sorting as ordered pair. the -sortid flag overrides this flag.<br>
*<br>
* -s | size <containter size><br>
*      (optional)  Integer which specifies how large the replacement selection sort
*                  container will be. Defaults to size 8<br>
*/
int main(int argc, char** argv) {
    if (argc < MIN_ARG_CNT){
        printf("ERROR: Not enough arguments\n");
        return ERR_ARG_CNT;
    }

    // Parse cmd line arguments
    for (int i = ONE; i < argc; i++) {

        if (!strcmp(argv[i], "-dir") || !strcmp(argv[i], "-d")) {
            i++;

            if (argv[i] == NULL || !strncmp(argv[i], "-", ONE)) {
                printf("ERROR: invalid argument found for -dir|d\n");
                return ERR_ARG_VAL;
            }

            strcpy(filename, argv[i]);
            printf("Data file is [%s]...\n", filename);
        }
        else if (!strcmp(argv[i], "-size") || !strcmp(argv[i], "-s")) {
            i++;

            if (argv[i] == NULL || !strncmp(argv[i], "-", ONE)) {
                printf("ERROR: invalid argument found for -size|s\n");
                return ERR_ARG_VAL;
            }

            containerSize = atoi(argv[i]);
            printf("Setting container to size [%d]...\n", containerSize);
        }
        else if (!strcmp(argv[i], "-sortname")) {
            printf("Sorting by name...\n");
            sortByName             = true;
            currentResult.sortName = true;
            data.sortName          = true;
        }
        else if (!strcmp(argv[i], "-sortid")) {
            printf("Sorting by ID...\n");
            sortByID              = true;
            currentResult.sortInt = true;
            data.sortInt          = true;
        }
        else {
            printf("ERROR: invalid argument [%s]\n", argv[i]);
            return ERR_ARG_VAL;
        }

    }

    printf("Starting...\n");

    readFile.open(filename);

    while (!container.currIsFull()) {
	totalRec++;
        readFile >> data.id >> data.name;
        container.current_heap_push(data);
    }

    writeFile.open(repselFilename);
    rrnVect.push_back(writeFile.tellp());

    while (readFile >> data.id >> data.name) {
	totalRec++;
        dataOut = container.current_heap_pop();
        writeFile << dataOut.id << " " << dataOut.name << endl;
        numOfData++;

        if (data < dataOut) {
            container.increase_pending();
            container.pending_heap_push(data);
        }
        else {
            container.current_heap_push(data);
        }

        if (container.currentSize() == ZERO) {
            container.switch_heap();

            numOfRuns++;
            numOfRecVector.push_back(numOfData);
            numOfData = ZERO;
            rrnVect.push_back(writeFile.tellp());
        }
    }
    readFile.close();


    if (!container.isEmpty()) {
        while (!container.currIsEmpty()) {
            dataOut = container.current_heap_pop();
            writeFile << dataOut.id << " " << dataOut.name << endl;
            numOfData++;
        }

        numOfRuns++;
        numOfRecVector.push_back(numOfData);
        numOfData = ZERO;

        rrnVect.push_back(writeFile.tellp());

        container.switch_heap();

        if (!container.currIsEmpty()) {
            while (!container.currIsEmpty()) {
                dataOut = container.current_heap_pop();
                writeFile << dataOut.id << " " << dataOut.name << endl;
                numOfData++;
            }

            numOfRuns++;
            numOfRecVector.push_back(numOfData);

            rrnVect.push_back(writeFile.tellp());
        }
    }
    writeFile.close();

    for (int i = 0; i < numOfRuns; i++) {
    	averageNumOfRec += (float) numOfRecVector[i];

    	if (numOfRecVector[i] < smallestRec)
    		smallestRec = numOfRecVector[i];

    	if (numOfRecVector[i] > largestRec)
    		largestRec = numOfRecVector[i];
    }

    averageNumOfRec = (averageNumOfRec / (float) numOfRuns);

    writeFile.open(loggerFilename);
    writeFile << "Unsorted input file: " << filename << endl;
    writeFile << "Sorted output file: " << kwayFilename << endl;
    writeFile << "Number of records that can fit in memory: " << MAX_CONTAINER_SIZE << endl;
    writeFile << "Number of Records: " << totalRec << endl;
    writeFile << "Number of Runs: " << numOfRuns << endl;
    writeFile << "Smallest Record Size: " << smallestRec << endl;
    writeFile << "Largest Record Size: " << largestRec << endl;
    writeFile << "Average Record Size: " << averageNumOfRec << endl;

    writeFile.close();

    printf("Statistics written to log file...\n");

    printf("Done...\n");
    readFile.open(repselFilename);

    ofstream sorted_rec(kwayFilename);
    mergeKWay(rrnVect, numOfRecVector, numOfRuns, containerSize, totalRec, largestRec, readFile, sorted_rec);

    return 0;
}


//===================fucntion=========================

/**
 * @brief K way tournament the replacement selection results
 * @param[in] rrn the relative record number of the record to be accessed
 * @param[in] numOfRecVector the number of rrns there is
 * @param[in] num_of_runs the total amount of time the replacement selection was ran
 * @param[in] record_in_runs the number of record in each rrn
 * @param[in] total_runs the total amount of records
 * @param[in] largeRec the largest record in the set of data
 * @param[in] in_file the replacement selection results file
 * @param[in] out_file the file that outputs the results of the kway
 */
template <typename T>
void mergeKWay(std::vector<T> rrn, std::vector<int> numOfRecVector, int num_of_runs, int record_in_runs, int total_runs, int largeRec, ifstream &in_file, ofstream &out_file){

    int total_index = total_runs;
    int curpos[num_of_runs];

    b2bHeap<fileData> k_heap(num_of_runs);
    
    fileData root;
    fileData index;
    
    if (sortByName) {
        root.sortName  = true;
        index.sortName = true;
    }
    if (sortByID) {
        root.sortInt  = true;
        index.sortInt = true;
    }

    for(int i = 0; i < num_of_runs; i++)
    {
        //grab the first element of all vectors
        in_file.seekg(rrn[i]);
        in_file >> index.id >> index.name;
        k_heap.current_heap_push(index);
        curpos[i] = 1;
    }

    for(int count = 0; count < total_index; count++)
    {
        //reset the min position
        int minpos = -1;

        root = k_heap.current_heap_pop();
        out_file << root.id << " " << root.name << endl;

        //reset the min to be the max int to find newest min
        root.clear_data();

        for(int i = 0; i < num_of_runs; i++)
        {
            if(curpos[i] < numOfRecVector[i])
            {
                in_file.seekg(rrn[i]);
                for(int j = 0; j < curpos[i]; j++)
                {
                    in_file.ignore(largeRec, '\n');
                }
                in_file >> index.id >> index.name;
                if(index < root)
                {
                    root = index;
                    minpos = i;
                }
            }
        }

        curpos[minpos]++;
        k_heap.current_heap_push(root);

    }
}
